//
//  StoryTVC.swift
//  NewYourkTimes
//
//  Created by Mouayyad Taja on 1/10/21.
//

import UIKit
import SDWebImage

protocol StoryDelegate{
    func didClickBookmark(forStory story: Story, atIndex indexPath: IndexPath)
    func didClickItem(forStory story: Story, atIndex indexPath: IndexPath)
}

class StoryTVC: UITableViewCell {
    
    //Outlets
    @IBOutlet weak var titleLbl: UILabel?
    @IBOutlet weak var dateLbl: UILabel?
    @IBOutlet weak var imgView: UIImageView?
    @IBOutlet weak var imgViewHeightConstraint: NSLayoutConstraint?
    @IBOutlet weak var bookmarkBtn: UIButton?
    
    internal var aspectConstraint : NSLayoutConstraint? {
        didSet {
            if oldValue != nil {
                imgView?.removeConstraint(oldValue!)
            }
            if aspectConstraint != nil {
                aspectConstraint?.priority = UILayoutPriority(999)
                imgView?.addConstraint(aspectConstraint!)
            }
        }
    }
    
    //Properties
    var story:Story?{
        didSet {
            self.titleLbl?.text = story?.title
            self.dateLbl?.text = story?.dateTime
            
            if let media = story?.multimedia?.first{
                self.setupImage(media: media)
            }
            self.isBookmarked = story?.isBookmarked ?? false
        }
    }
    
    var delegate:StoryDelegate?
    var indexPath:IndexPath?
    
    var isBookmarked:Bool = false {
        didSet {
            let image = isBookmarked ? UIImage(systemName: "bookmark.fill") :  UIImage(systemName: "bookmark")
            self.bookmarkBtn?.setImage(image, for: .normal)
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //Observer changing on bookmark
        NotificationCenter.default.addObserver(self, selector: #selector(onAddToBookmark(_:)), name: .didAddToBookmark, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onDeleteBookmark(_:)), name: .didDeleteFromBookmark, object: nil)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        aspectConstraint = nil
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func setupImage(media: Multimedia){
        guard  let imageView = imgView else {return}
        let aspectRatio = media.getRatio()
        self.aspectConstraint = NSLayoutConstraint(item: imageView, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: imageView, attribute: NSLayoutConstraint.Attribute.height, multiplier: aspectRatio, constant: 0.0)
        self.imgView?.sd_setImage(with: URL(string: media.url), placeholderImage: UIImage(named: "placeholder.png"))
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
    
    @IBAction func didClickBookmarkAction(){
        guard let indexPath = self.indexPath, let story = self.story else {return}
        self.delegate?.didClickBookmark(forStory: story, atIndex: indexPath)
    }
    
    //Hanlde DB updates on item
    @objc func onAddToBookmark(_ notification:Notification){
        if let story = notification.object as? Story, story.uri == self.story?.uri {
            self.story?.isBookmarked = true
            self.isBookmarked = true
        }
    }
    
    //Hanlde DB updates on item
    @objc func onDeleteBookmark(_ notification:Notification){
        if let story = notification.object as? Story, story.uri == self.story?.uri {
            self.story?.isBookmarked = false
            self.isBookmarked = false
        }
    }
}
