//
//  StoryCVC.swift
//  NewYourkTimes
//
//  Created by Mouayyad Taja on 1/11/21.
//

import UIKit
import SDWebImage

class StoryCVC: UICollectionViewCell {

    //Outlets
    @IBOutlet weak var titleLbl: UILabel?
    @IBOutlet weak var dateLbl: UILabel?
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var bookmarkBtn: UIButton?

    
    //Properties
    var story:Story?{
        didSet {
            self.titleLbl?.text = story?.title
            self.dateLbl?.text = story?.dateTime
            
            if let media = story?.multimedia?.first{
                self.imgView?.sd_setImage(with: URL(string: media.url), placeholderImage: UIImage(named: "placeholder.png"))
            }
            self.isBookmarked = story?.isBookmarked ?? false
        }
    }
    
    var delegate:StoryDelegate?
    var indexPath:IndexPath?


    var isBookmarked:Bool = false {
        didSet {
            let image = isBookmarked ? UIImage(systemName: "bookmark.fill") :  UIImage(systemName: "bookmark")
            self.bookmarkBtn?.setImage(image, for: .normal)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func didClickBookmarkAction(){
        guard let indexPath = self.indexPath, let story = self.story else {return}
        self.delegate?.didClickBookmark(forStory: story, atIndex: indexPath)
    }
}
