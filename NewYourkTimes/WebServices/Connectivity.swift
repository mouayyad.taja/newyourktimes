//
//  Connectivity.swift
//  NewYourkTimes
//
//  Created by Mouayyad Taja on 11/9/20.
//

import Foundation
import Alamofire

class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}
