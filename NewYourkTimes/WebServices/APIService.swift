//
//  APIService.swift
//  NewYourkTimes
//
//  Created by Mouayyad Taja on 11/7/20.
//

import Foundation
import Alamofire

public typealias ResultHandler<T> = (Result<T, Error>)->Void

struct APIService {
    
    //Get list of items
    func getItems(_ completion: (ResultHandler<[Story]>)? = nil){
       
        let urlString = "https://api.nytimes.com/svc/topstories/v2/automobiles.json?api-key=Bcq5SArjOhlpNYzXVn39u4FoqganQM0K"
        
        let request = AF.request(urlString, method: .get, parameters: [:])
        
//        //No iternet connection
//        if !Connectivity.isConnectedToInternet {
//
////            //Get items from DB
////            let items = DBHelper.shared.getFeedItems(filterTitle: filterTitle)
//            completion?(Result.success([]))
//            return
//        }
        
        request.responseDecodable(of: GenericData<Story>.self){ (response) in
            switch response.result {
            case .success(let data):
                let items = data.results ?? []
//                DBHelper.shared.insertItems(newItems: data)
//                let items = DBHelper.shared.getFeedItems(filterTitle: filterTitle)
                completion?(Result.success(items))
            case .failure(let error):
               
//                //Get items from DB
//                let items = DBHelper.shared.getFeedItems(filterTitle: filterTitle)
//                if !items.isEmpty {
//                    completion?(Result.success(items))
//                    return
//                }
                completion?(Result.failure(error))
                return
            }
        }
    }
    

}
