//
//  BookmarksDataSource.swift
//  NewYourkTimes
//
//  Created by Mouayyad Taja on 1/11/21.
//

import Foundation
import UIKit
import SwifterSwift

class BookmarksDataSource: GenericDataSource<Story>{
    
    //Properties
    var delegate: StoryDelegate?
    
    //MARK: Initialization
    init(delegate: StoryDelegate?) {
        self.delegate = delegate
    }
    
}

//MARK: UICollectionViewDelegate & UICollectionViewDataSource & UICollectionViewDelegateFlowLayout
extension BookmarksDataSource: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.value.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: StoryCVC.self, for: indexPath)
        let item = self.data.value[indexPath.row]
        cell.story = item
        cell.indexPath = indexPath
        cell.delegate = self.delegate
        return cell
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
        let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) +
            (flowayout?.sectionInset.horizontal ?? 0.0)
        let width:CGFloat = (collectionView.frame.size.width - space) / 2.0
        let size = CGSize(width: width, height: 300)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = self.data.value[indexPath.row]
        self.delegate?.didClickItem(forStory: item, atIndex: indexPath)
    }
    
}

