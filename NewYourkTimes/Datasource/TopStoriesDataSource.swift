//
//  TopStoriesDataSource.swift
//  NewYourkTimes
//
//  Created by Mouayyad Taja on 1/11/21.
//

import Foundation
import UIKit
import SwifterSwift

class TopStoriesDataSource: GenericDataSource<Story>{
    
    //Properties
    var delegate: StoryDelegate?
    
    //MARK: Initialization
    init(delegate: StoryDelegate?) {
        self.delegate = delegate
    }
    
}

//MARK: UITableViewDelegate & UITableViewDataSource
extension TopStoriesDataSource: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: StoryTVC.self, for: indexPath)
        let item = self.data.value[indexPath.row]
        cell.story = item
        cell.indexPath = indexPath
        cell.delegate = self.delegate
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = self.data.value[indexPath.row]
        self.delegate?.didClickItem(forStory: item, atIndex: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
