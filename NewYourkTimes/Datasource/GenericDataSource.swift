//
//  GenericDataSource.swift
//  NewYourkTimes
//
//  Created by Mouayyad Taja on 1/11/21.
//

import Foundation
class GenericDataSource<T>: NSObject {
    var data: DynamicValue<[T]> = DynamicValue([])
}
