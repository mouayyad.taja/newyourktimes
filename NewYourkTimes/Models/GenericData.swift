//
//  GenericData.swift
//  NewYourkTimes
//
//  Created by Mouayyad Taja on 1/11/21.
//

import Foundation

// MARK: - GenericData
class GenericData<TYPE: Codable>: Codable {
    var status, copyright, section: String?
    var lastUpdated: String?
    var numResults: Int?
    var results: [TYPE]?

    enum CodingKeys: String, CodingKey {
        case status, copyright, section
        case lastUpdated = "last_updated"
        case numResults = "num_results"
        case results
    }

    init(status: String?, copyright: String?, section: String?, lastUpdated: String?, numResults: Int?, results: [TYPE]?) {
        self.status = status
        self.copyright = copyright
        self.section = section
        self.lastUpdated = lastUpdated
        self.numResults = numResults
        self.results = results
    }
}

// MARK: GenericData convenience initializers and mutators

extension GenericData {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(GenericData.self, from: data)
        self.init(status: me.status, copyright: me.copyright, section: me.section, lastUpdated: me.lastUpdated, numResults: me.numResults, results: me.results)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        status: String?? = nil,
        copyright: String?? = nil,
        section: String?? = nil,
        lastUpdated: String?? = nil,
        numResults: Int?? = nil,
        results: [TYPE]?? = nil
    ) -> GenericData {
        return GenericData(
            status: status ?? self.status,
            copyright: copyright ?? self.copyright,
            section: section ?? self.section,
            lastUpdated: lastUpdated ?? self.lastUpdated,
            numResults: numResults ?? self.numResults,
            results: results ?? self.results
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
