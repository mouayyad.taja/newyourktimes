//
//  Story.swift
//  NewYourkTimes
//
//  Created by Mouayyad Taja on 1/10/21.
//

import Foundation
import AFDateHelper

// MARK: - Story
class Story: Codable {
    var title, abstract: String?
    var url: String?
    var uri: String?
    var createdDate: String?
    var multimedia: [Multimedia]?

    
    enum CodingKeys: String, CodingKey {
        case title, abstract, url, uri
        case createdDate = "created_date"
        case multimedia
    }

    init(title: String?, abstract: String?, url: String?, uri: String?, createdDate: String?, multimedia: [Multimedia]?) {
        self.title = title
        self.abstract = abstract
        self.url = url
        self.uri = uri
        self.createdDate = createdDate
        self.multimedia = multimedia
    }
    
    lazy var dateTime:String = {
        if let createdDate = Date(fromString: self.createdDate ?? "", format: .isoDateTimeSec){
            let str = createdDate.string()
            return str
        }
        return ""
    }()
    
    lazy var isBookmarked: Bool = {
        return DBHelper.shared.isBookmarked(story: self)
    }()
}

// MARK: Story convenience initializers and mutators

extension Story {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(Story.self, from: data)
        self.init(title: me.title, abstract: me.abstract, url: me.url, uri: me.uri, createdDate: me.createdDate, multimedia: me.multimedia)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        title: String?? = nil,
        abstract: String?? = nil,
        url: String?? = nil,
        uri: String?? = nil,
        createdDate: String?? = nil,
        multimedia: [Multimedia]?? = nil
    ) -> Story {
        return Story(
            title: title ?? self.title,
            abstract: abstract ?? self.abstract,
            url: url ?? self.url,
            uri: uri ?? self.uri,
            createdDate: createdDate ?? self.createdDate,
            multimedia: multimedia ?? self.multimedia
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - Multimedia
class Multimedia: Codable {
    var url: String?
    var format: String?
    var height, width: Int?
    var type, subtype, caption, copyright: String?

    init(url: String?, format: String?, height: Int?, width: Int?, type: String?, subtype: String?, caption: String?, copyright: String?) {
        self.url = url
        self.format = format
        self.height = height
        self.width = width
        self.type = type
        self.subtype = subtype
        self.caption = caption
        self.copyright = copyright
    }
    
    func getRatio()->CGFloat{
        var height: CGFloat = 100
        let mediaWidth = CGFloat(self.width ?? 0)
        let mediaHeight = CGFloat(self.height ?? 0)
        if mediaWidth > 0 {
            return mediaWidth/mediaHeight
        }
        return 1
    }
    
    func getHeight(width: CGFloat)->CGFloat{
        var height: CGFloat = 100
        let mediaWidth = CGFloat(self.width ?? 0)
        let mediaHeight = CGFloat(self.height ?? 0)
        if mediaWidth > 0 {
            height = mediaHeight / mediaWidth * width
        }
        return height
    }

}

// MARK: Multimedia convenience initializers and mutators

extension Multimedia {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(Multimedia.self, from: data)
        self.init(url: me.url, format: me.format, height: me.height, width: me.width, type: me.type, subtype: me.subtype, caption: me.caption, copyright: me.copyright)
    }

    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }

    func with(
        url: String?? = nil,
        format: String?? = nil,
        height: Int?? = nil,
        width: Int?? = nil,
        type: String?? = nil,
        subtype: String?? = nil,
        caption: String?? = nil,
        copyright: String?? = nil
    ) -> Multimedia {
        return Multimedia(
            url: url ?? self.url,
            format: format ?? self.format,
            height: height ?? self.height,
            width: width ?? self.width,
            type: type ?? self.type,
            subtype: subtype ?? self.subtype,
            caption: caption ?? self.caption,
            copyright: copyright ?? self.copyright
        )
    }

    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
