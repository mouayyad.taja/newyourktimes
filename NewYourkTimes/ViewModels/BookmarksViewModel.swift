//
//  BookmarksViewModel.swift
//  NewYourkTimes
//
//  Created by Mouayyad Taja on 1/11/21.
//

import Foundation
import SwifterSwift

class BookmarksViewModel {
    
    //Api reference
    private var apiService : APIService!
    
    //Observable data
    private(set) var items : DynamicValue<[Story]> = DynamicValue<[Story]>([])

    //Observable error
    private(set) var error : DynamicValue<Error?> = DynamicValue<Error?>(nil)
    
    
    //MARK: Initialization
    init() {
        self.apiService = APIService()
        NotificationCenter.default.addObserver(self, selector: #selector(onUpdateData(_:)), name: .didAddToBookmark, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onUpdateData(_:)), name: .didDeleteFromBookmark, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    //Get list stories
    func getItems(filterTitle: String? = nil){
        let items = DBHelper.shared.getItems()
        self.items.value = items
    }
    
    
    //Bookmark story
    func bookmarkStory(story: Story){
        if !story.isBookmarked {
            DBHelper.shared.insertItem(story: story)
        }else {
            DBHelper.shared.deletedItem(story: story)
        }
    }
    
    //Hanlde DB updates on trash items
    @objc func onUpdateData(_ notification:Notification){
        self.items.value = DBHelper.shared.getItems()
    }
}


