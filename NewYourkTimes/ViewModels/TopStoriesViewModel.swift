//
//  TopStoriesViewModel.swift
//  NewYourkTimes
//
//  Created by Mouayyad Taja on 1/11/21.
//

import Foundation
import SwifterSwift

class TopStoriesViewModel {
    
    //Api reference
    private var apiService : APIService!
    
    //Observable data
    private(set) var items : DynamicValue<[Story]> = DynamicValue<[Story]>([])

    //Observable error
    private(set) var error : DynamicValue<Error?> = DynamicValue<Error?>(nil)
    
    
    //MARK: Initialization
    init() {
        self.apiService = APIService()
    }
    
    
    
    //Get list stories
    func getItems(filterTitle: String? = nil, _ completion: ((Result<[Story], Error>)->Void)? = nil){
        self.apiService.getItems(){
            result in
            switch result {
                case .success(let items):
                    self.items.value = items
                    completion?(result)
                case .failure(let error):
                    print("error \(error)")
                    self.error.value = error
                    completion?(result)
            }
        }
    }
    
    
    //Bookmark story
    func bookmarkStory(story: Story){
        if !story.isBookmarked {
            DBHelper.shared.insertItem(story: story)
        }else {
            DBHelper.shared.deletedItem(story: story)
        }
    }
    
}

