//
//  StoryDetailsViewModel.swift
//  NewYourkTimes
//
//  Created by Mouayyad Taja on 1/11/21.
//

import Foundation
import SwifterSwift

class StoryDetailsViewModel {
    
    //data
    private(set) var item : Story

    var onUpdateStory: (()->Void)?
    
    //MARK: Initialization
    init(item : Story) {
        self.item = item
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(onAddToBookmark(_:)), name: .didAddToBookmark, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onDeleteBookmark(_:)), name: .didDeleteFromBookmark, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    //Bookmark story
    func bookmarkStory(){
        if !item.isBookmarked {
            DBHelper.shared.insertItem(story: item)
        }else {
            DBHelper.shared.deletedItem(story: item)
        }
    }
    
    
    //Hanlde DB updates on item
    @objc func onAddToBookmark(_ notification:Notification){
        if let story = notification.object as? Story, story.uri == self.item.uri {
            self.item.isBookmarked = true
            self.onUpdateStory?()
        }
    }
    
    //Hanlde DB updates on item
    @objc func onDeleteBookmark(_ notification:Notification){
        if let story = notification.object as? Story, story.uri == self.item.uri {
            self.item.isBookmarked = false
            self.onUpdateStory?()
        }
    }
}



