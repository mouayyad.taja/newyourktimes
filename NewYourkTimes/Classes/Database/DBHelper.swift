//
//  DBHelper.swift
//  NewYourkTimes
//
//  Created by Mouayyad Taja on 11/9/20.
//

import Foundation
import SQLite
import SQLite3


class DBHelper {
    
    private static var dbHelper = DBHelper()
    
    static var shared:DBHelper {
        return dbHelper
    }
    
    //Properties
    var path: String? {
        let directoryUrl = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        return directoryUrl?.appendingPathComponent("db.sqlite").relativePath
    }
    
    var db :Connection?
    
    let story = Table("story")
    
    let id = Expression<String>("uri")
    let title = Expression<String?>("title")
    let abstract = Expression<String>("abstract")
    let created_date = Expression<String>("created_date")
    let url = Expression<String?>("url")
    
    
    let media = Table("media")
    
    let mediaStoryId = Expression<String>("story_id")
    let mediaUrl = Expression<String?>("url")
    let mediaWidth = Expression<Int>("width")
    let mediaHeight = Expression<Int>("height")
    
    //MARK: Initialization
    init() {
        db = openDatabase()
        createTable()
    }
    
    //Connect to DB
    func openDatabase() -> Connection? {
        guard let dbPath = path else {
            print("part1DbPath is nil.")
            return nil
        }
        do {
            let db = try Connection(dbPath)
            return db
        }catch (let error){
            print(error)
            return nil
        }
    }
    
    //Create table if isn't exist
    func createTable() {
        do {
            try db?.run(story.create{ t in
                t.column(id, unique: true)
                t.column(title)
                t.column(abstract)
                t.column(created_date)
                t.column(url)
            })
            
            try db?.run(media.create{ t in
                t.column(mediaStoryId, unique: true)
                t.column(url)
                t.column(mediaWidth)
                t.column(mediaHeight)
            })
        }catch (let error){
            print(error)
        }
    }
    
    //Inesert a new story
    func insertItem(story: Story){
        guard let db = db else { return }
        do {
            try db.transaction {
                try db.run(self.story.insert(or: .ignore, [id <- story.uri!,
                                                                       title <- story.title!,
                                                                       abstract <- story.abstract!,
                                                                       created_date <- story.createdDate!,
                                                                       url <- story.url
                ]))
                if let media = story.multimedia?.first {
                    try db.run(self.media.insert(or: .ignore, [mediaStoryId <- story.uri!,
                                                               mediaUrl <- media.url!,
                                                               mediaWidth <- media.width!,
                                                               mediaHeight <- media.height!
                    ]))
                }
            }
            
            NotificationCenter.default.post(name: .didAddToBookmark, object: story)
        }catch (let error){
            print("insertItem error")
            print(error)
        }
    }
    
    
    //Get items
    func getItems()-> [Story]{
        guard let db = db else { return [] }
        var list = [Story]()
        do {
            let table = self.story
            for row in try db.prepare(table){
                let item = Story(title: row[title], abstract: row[abstract], url: row[url], uri: row[id], createdDate: row[created_date], multimedia: [])
                
                var mediaTable = self.media
                mediaTable = mediaTable.filter(mediaStoryId == item.uri!)
                for row in try db.prepare(mediaTable){
                    let mediaItem = Multimedia(url: row[url], format: "", height: row[mediaHeight], width: row[mediaWidth], type: "", subtype: "", caption: "", copyright: "")
                    item.multimedia = [mediaItem]
                }
                list.append(item)
            }
        }catch (let error){
            print("getItems error")
            print(error)
        }
        return list
    }
    
    //Delete item
    func deletedItem(story: Story){
        guard let db = db else { return }
        do {
            let table = self.story.filter(id == story.uri!)
            if try db.run(table.delete()) == 1 {
                print("Item deleted successfully")
                NotificationCenter.default.post(name: .didDeleteFromBookmark, object: story)
            }else {
                print("alice not found")
            }
        }catch (let error){
            print("deletedItem error")
            print(error)
        }
    }
    
    
    
    //Check if story is bookmarked
    func isBookmarked(story: Story)->Bool{
        guard let db = db else { return false}
        do {
            let table = self.story.filter(id.like(story.uri ?? ""))
            let totalCount = try db.scalar(table.count)
            if totalCount > 0 {
                return true
            }
        }catch (let error){
            print("isBookmarked error")
            print(error)
        }
        return false
    }
}

//Define a new types of Notification
extension Notification.Name {
    static let didAddToBookmark = Notification.Name("didAddToBookmark")
    static let didDeleteFromBookmark = Notification.Name("didDeleteFromBookmark")
}
