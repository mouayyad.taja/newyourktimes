//
//  StoryDetailsVC.swift
//  NewYourkTimes
//
//  Created by Mouayyad Taja on 1/11/21.
//

import UIKit
import SwifterSwift

class StoryDetailsVC: UIViewController {

    //Outlets
    @IBOutlet weak var titleLbl: UILabel?
    @IBOutlet weak var abstractLbl: UILabel?
    @IBOutlet weak var dateLbl: UILabel?
    @IBOutlet weak var imgView: UIImageView?
    @IBOutlet weak var imgViewHeightConstraint: NSLayoutConstraint?
    @IBOutlet weak var bookmarkBtn: UIButton?

    //Properties
    var story:Story?
    private var viewModel : StoryDetailsViewModel?

    //MARK: Initialize view model
    func prepareViewModel(){
        guard let story = self.story else {return}
        
        self.viewModel = StoryDetailsViewModel(item: story)
        
        //Observe data
        self.viewModel?.onUpdateStory = {
            self.fillData()
        }
        
        fillData()
    }
    
    
    //MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        prepareViewModel()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    
    //MARK: Fill Data
    func fillData(){
        guard let story = self.viewModel?.item else {return}

        self.titleLbl?.text = story.title
        self.dateLbl?.text = story.dateTime
        self.abstractLbl?.text = story.abstract
        if let media = story.multimedia?.first{
            let height = media.getHeight(width: self.imgView?.frame.width ?? 0)
            self.imgViewHeightConstraint?.constant = height
            self.imgView?.sd_setImage(with: URL(string: media.url), placeholderImage: UIImage(named: "placeholder.png"))
            self.view.layoutIfNeeded()
        }
        
        self.updateBookmarkedBtn()
    }
    
    func updateBookmarkedBtn(){
        guard let story = self.viewModel?.item else {return}

        let isBookmarked = story.isBookmarked
        let image = isBookmarked ? UIImage(systemName: "bookmark.fill") :  UIImage(systemName: "bookmark")
        self.bookmarkBtn?.setImage(image, for: .normal)
    }
    

    
    @IBAction func didClickBookmarkAction(){
        self.viewModel?.bookmarkStory()
    }
    
    @IBAction func didClickWebsiteAction(){
        guard let story = self.viewModel?.item else {return}
        if let urlStr = story.url, let url = URL(string: urlStr) {
            UIApplication.shared.openURL(url)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
