//
//  BookmarksVC.swift
//  NewYourkTimes
//
//  Created by Mouayyad Taja on 1/10/21.
//

import UIKit

class BookmarksVC: BaseVC {
    
    //Outlets
    @IBOutlet weak var collectionView: UICollectionView?
    
    //Properties
    private var viewModel : BookmarksViewModel!
    private lazy var dataSource : BookmarksDataSource = {
        return BookmarksDataSource(delegate: self)
    }()

    //MARK: Initialize view model
    func prepareViewModel(){
        self.viewModel = BookmarksViewModel()
        
        //Observe data
        setupObservable()
        
        //Get data
        getData()
    }
    
    
    //MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        //Set navigation title
        self.navigationItem.title = ""
        
        prepareViewModel()
    }
    
    //MARK: Setup UI
    override func setupViews(){
        setupCollectionView()
        createStateView(view: self.collectionView)
    }

    //Prepare collection view properities
    func setupCollectionView(){
        guard let collectionView = collectionView else {return}
        collectionView.delegate = self.dataSource
        collectionView.dataSource = self.dataSource
        collectionView.register(nibWithCellClass: StoryCVC.self)
        let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
        layout?.minimumLineSpacing = 10
        layout?.minimumInteritemSpacing = 10
        layout?.sectionInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        self.collectionView?.reloadData()
    }
    
    
    //Observe data
    func setupObservable(){
        //observe items changes from view model
        self.viewModel.items.addObserver(observer: self) { (items) in
            self.dataSource.data.value = items
            self.collectionView?.reloadData()
            
            if items.isEmpty {
                //Show state view empty
                self.stateView?.setStateNoDataFound()
            }else {
                //hide state view
                self.stateView?.setDataAvailable()
            }
        }
        
        //observe error change from view model
        self.viewModel.error.addObserver(observer: self) { (error) in
            if self.dataSource.data.value.isEmpty {
                //show error state view
                if let message = error?.localizedDescription {
                    self.stateView?.setServerError(title: "Error", message: message, retryAction: self.getData)
                }
            }else {
                //show error alert
                self.showAlert(title: "Error", message: error?.localizedDescription)
            }
        }
    }
    
    // MARK: - GetData
    func getData(){
        //Show state view loading
        self.stateView?.setStateLoading()
        
        //fetch list items
        viewModel.getItems()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - StoryDelegate
extension BookmarksVC: StoryDelegate {
    func didClickBookmark(forStory story: Story, atIndex indexPath: IndexPath) {
        self.viewModel.bookmarkStory(story: story)
    }

    func didClickItem(forStory story: Story, atIndex indexPath: IndexPath) {
        let vc = ControllerManager.main.storyDetailsVC
        vc.story = story
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

