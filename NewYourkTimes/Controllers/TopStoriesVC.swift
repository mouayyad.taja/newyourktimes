//
//  TopStoriesVC.swift
//  NewYourkTimes
//
//  Created by Mouayyad Taja on 1/10/21.
//

import UIKit

class TopStoriesVC: BaseVC {

    //Outlets
    @IBOutlet weak var tableView: UITableView?
    
    //Properties
    private var viewModel : TopStoriesViewModel!
    private lazy var dataSource : TopStoriesDataSource = {
        return TopStoriesDataSource(delegate: self)
    }()

    //MARK: Initialize view model
    func prepareViewModel(){
        self.viewModel = TopStoriesViewModel()
        
        
        //Observe data
        setupObservable()
        
        //Get data
        getData()
    }
    
    
    //MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        //Set navigation title
        self.navigationItem.title = "Automobiles"
        
        prepareViewModel()
    }
    
    //MARK: Setup UI
    override func setupViews(){
        setupTableView()
        createStateView(view: tableView)
    }

    //Prepare table view properities
    func setupTableView(){
        guard let tableView = tableView else {return}
        tableView.delegate = self.dataSource
        tableView.dataSource = self.dataSource
        tableView.register(nibWithCellClass: StoryTVC.self)
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
        tableView.reloadData()
    }
    
    
    //Observe data
    func setupObservable(){
        //observe items changes from view model
        self.viewModel.items.addObserver(observer: self) { (items) in
            self.dataSource.data.value = items
            self.tableView?.reloadData()
            
            if items.isEmpty {
                //Show state view empty
                self.stateView?.setStateNoDataFound()
            }else {
                //hide state view
                self.stateView?.setDataAvailable()
            }
        }
        
        //observe error change from view model
        self.viewModel.error.addObserver(observer: self) { (error) in
            if self.dataSource.data.value.isEmpty {
                //show error state view
                if let message = error?.localizedDescription {
                    self.stateView?.setServerError(title: "Error", message: message, retryAction: self.getData)
                }
            }else {
                //show error alert
                self.showAlert(title: "Error", message: error?.localizedDescription)
            }
        }
    }
    
    // MARK: - GetData
    func getData(){
        //Show state view loading
        self.stateView?.setStateLoading()
        
        //fetch list items
        viewModel.getItems()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - StoryDelegate
extension TopStoriesVC: StoryDelegate {
    func didClickBookmark(forStory story: Story, atIndex indexPath: IndexPath) {
        self.viewModel.bookmarkStory(story: story)
    }

    func didClickItem(forStory story: Story, atIndex indexPath: IndexPath) {
        let vc = ControllerManager.main.storyDetailsVC
        vc.story = story
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

